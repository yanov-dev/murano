﻿using System;

namespace Murano.Models
{
    public class TimeRange
    {
        public DateTime From { get; }
        
        public DateTime To { get; }
        
        public TimeRange(DateTime dateFrom, DateTime dateTo)
        {
            if (dateTo <= dateFrom)
                throw new ArgumentException("dateTo must more than dateFrom");

            From = dateFrom;
            To = dateTo;
        }

        public bool InRange(DateTime dateTime)
        {
            return dateTime >= From && dateTime <= To;
        }
    }
}