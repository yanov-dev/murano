﻿using System.Collections.Generic;

namespace Murano.Models
{
    public class GraphVertex
    {
        public string Name { get; }

        public int Used { get; set; }

        private readonly HashSet<GraphEdge> _edges;

        public IEnumerable<GraphEdge> Edges
        {
            get { return _edges; }
        }

        public GraphVertex(string name)
        {
            Name = name;
            _edges = new HashSet<GraphEdge>();
        }

        public void AddEdge(GraphEdge edge)
        {
            if (_edges.Contains(edge))
                return;

            _edges.Add(edge);
        }

        public override int GetHashCode()
        {
            var hash = Name.GetHashCode();
            return hash;
        }
    }
}