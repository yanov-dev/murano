﻿using System;

namespace Murano.Models
{
    public class GraphEdge
    {
        public GraphVertex VertexLeft { get; }
        public GraphVertex Vertexright { get; }

        public int Used { get; set; }

        public GraphEdge(GraphVertex vertexLeft, GraphVertex vertexright)
        {
            VertexLeft = vertexLeft;
            Vertexright = vertexright;
        }

        public override int GetHashCode()
        {
            return GetId().GetHashCode();
        }

        public GraphVertex GetOther(GraphVertex vertex)
        {
            if (vertex.Name.Equals(VertexLeft.Name))
                return Vertexright;

            return VertexLeft;
        }

        public string GetId()
        {
            var name1 = VertexLeft.Name;
            var name2 = Vertexright.Name;
            if (string.Compare(name1, name2, StringComparison.Ordinal) > 0)
                return $"{name1}_{name2}";
            return $"{name2}_{name1}";
        }
    }
}