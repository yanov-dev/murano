﻿using CommandLine;

namespace Murano.Models
{
    public class CommandLineOptions
    {
        [Option("file", Required = true, HelpText = "Path to source file")]
        public string Path { get; set; }
        
        [Option("date_from", Required = true, HelpText = "Date from")]
        public string DateFrom { get; set; }
        
        [Option("date_to", Required = true, HelpText = "Date to")]
        public string DateTo { get; set; }
    }
}