﻿using System;

namespace Murano.Models
{
    public class GraphAction
    {
        public GraphAction(DateTime time, string fromService, string toService)
        {
            if (string.IsNullOrWhiteSpace(fromService))
                throw new ArgumentNullException(nameof(fromService));
            if (string.IsNullOrWhiteSpace(toService))
                throw new ArgumentNullException(nameof(toService));

            Time = time;
            FromService = fromService;
            ToService = toService;
        }

        public DateTime Time { get; }

        public string FromService { get; }

        public string ToService { get; }
    }
}