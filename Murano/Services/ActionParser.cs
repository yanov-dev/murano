﻿using System;
using Murano.Models;

namespace Murano.Services
{
    public class ActionParser
    {
        public GraphAction Parse(string text)
        {
            var dateStr = text.Substring(0, 24);
            if (!DateTime.TryParse(dateStr, out var date))
                throw new Exception("date has incorect format");
            
            var nodes = text.Remove(0, 25).Split(' ');
            if (nodes.Length < 2)
                throw new Exception("text is invalid");
            
            return new GraphAction(date, nodes[0], nodes[1]);
        }
    }
}