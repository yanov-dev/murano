﻿using System;
using System.Collections.Generic;
using System.IO;
using Murano.Models;

namespace Murano.Services.GraphImporter
{
    public class FileGraphImporter : IGraphImporter
    {
        private readonly ActionParser _actionParser;
        private readonly string _fileName;

        public FileGraphImporter(string fileName, TimeRange timeRange, ActionParser actionParser)
        {
            if (timeRange == null)
                throw new ArgumentNullException(nameof(timeRange));
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentNullException(nameof(fileName));

            if (!File.Exists(fileName))
                throw new ArgumentException($"file {fileName} doesn't exists");

            _fileName = fileName;
            TimeRange = timeRange;
            _actionParser = actionParser ?? throw new ArgumentNullException(nameof(actionParser));
        }

        public TimeRange TimeRange { get; }

        public IEnumerable<GraphAction> GetAllActions()
        {
            using (var sr = new StreamReader(_fileName))
            {
                while (true)
                {
                    var line = sr.ReadLine();
                    if (line == null)
                        yield break;

                    var action = _actionParser.Parse(line);
                    if (TimeRange.InRange(action.Time))
                        yield return action;
                }
            }
        }
    }
}