﻿using System.Collections.Generic;
using Murano.Models;

namespace Murano.Services.GraphImporter
{
    public interface IGraphImporter
    {
        TimeRange TimeRange { get; }

        IEnumerable<GraphAction> GetAllActions();
    }
}