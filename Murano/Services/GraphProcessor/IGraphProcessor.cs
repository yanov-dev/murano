﻿using System.Collections.Generic;
using Murano.Models;

namespace Murano.Services.GraphProcessor
{
    public interface IGraphProcessor
    {
        void Process();

        IEnumerable<GraphVertex> Vertices { get; }
        
        IEnumerable<GraphEdge> Edges { get; }
    }
}