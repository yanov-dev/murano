﻿using System;
using System.Collections.Generic;
using Murano.Models;
using Murano.Services.GraphImporter;

namespace Murano.Services.GraphProcessor
{
    public class GraphProcessor : IGraphProcessor
    {
        private readonly IGraphImporter _importer;

        private readonly Dictionary<string, GraphVertex> _vertices;
        private readonly Dictionary<string, GraphEdge> _edges;

        public IEnumerable<GraphVertex> Vertices
        {
            get { return _vertices.Values; }
        }

        public IEnumerable<GraphEdge> Edges
        {
            get { return _edges.Values; }
        }

        public GraphProcessor(IGraphImporter importer)
        {
            _importer = importer ?? throw new ArgumentNullException(nameof(importer));

            _vertices = new Dictionary<string, GraphVertex>();
            _edges = new Dictionary<string, GraphEdge>();
        }

        public void Process()
        {
            _edges.Clear();
            _vertices.Clear();

            foreach (var action in _importer.GetAllActions())
            {
                var vertexLeft = AddOrIncrementVertex(new GraphVertex(action.FromService));
                var vertexRigth = AddOrIncrementVertex(new GraphVertex(action.ToService));

                var edge = AddOrIncrementEdge(new GraphEdge(vertexLeft, vertexRigth));

                vertexLeft.AddEdge(edge);
                vertexRigth.AddEdge(edge);
            }
        }

        private GraphEdge AddOrIncrementEdge(GraphEdge edge)
        {
            if (_edges.TryGetValue(edge.GetId(), out var v))
            {
                v.Used++;
                return v;
            }

            _edges[edge.GetId()] = edge;
            edge.Used++;
            return edge;
        }

        private GraphVertex AddOrIncrementVertex(GraphVertex vertex)
        {
            if (_vertices.TryGetValue(vertex.Name, out var v))
            {
                v.Used++;
                return v;
            }

            _vertices[vertex.Name] = vertex;
            vertex.Used++;
            return vertex;
        }
    }
}