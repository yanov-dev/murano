﻿using System;
using Murano.Services.GraphProcessor;

namespace Murano.Services.GraphDrawer
{
    public class ConsoleGraphDrawer : IGraphDrawer
    {
        public void Draw(IGraphProcessor graphProcessor)
        {
            Console.WriteLine("Graph");
            Console.WriteLine();
            foreach (var vertex in graphProcessor.Vertices)
            {
                Console.WriteLine($"{vertex.Name} (used {vertex.Used})");
                foreach (var edge in vertex.Edges)
                {
                    var otherVertex = edge.GetOther(vertex);
                    Console.WriteLine($"<--({edge.Used})--> {otherVertex.Name} (used {otherVertex.Used})");
                }
                Console.WriteLine();
            }
            
            Console.WriteLine("All vertexs");
            Console.WriteLine();

            foreach (var vertex in graphProcessor.Vertices)
                Console.WriteLine($"{vertex.Name} (used {vertex.Used})");
            
            Console.WriteLine();
            Console.WriteLine("All Edges");
            Console.WriteLine();
            
            foreach (var edge in graphProcessor.Edges)
                Console.WriteLine($"{edge.VertexLeft.Name}<-->{edge.Vertexright.Name} (Used {edge.Used})");
        }
    }
}