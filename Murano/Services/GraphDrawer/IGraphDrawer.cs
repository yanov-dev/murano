﻿using Murano.Services.GraphProcessor;

namespace Murano.Services.GraphDrawer
{
    public interface IGraphDrawer
    {
        void Draw(IGraphProcessor graphProcessor);
    }
}