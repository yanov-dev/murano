﻿using System;
using System.Linq;
using CommandLine;
using Murano.Models;
using Murano.Services;
using Murano.Services.GraphDrawer;
using Murano.Services.GraphImporter;
using Murano.Services.GraphProcessor;

namespace Murano
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineOptions>(args)
                .WithParsed(opts =>
                {
                    if (!DateTime.TryParse(opts.DateFrom, out var dateFrom))
                    {
                        Console.WriteLine("'Date from' has incorrect format");
                        return;
                    }

                    if (!DateTime.TryParse(opts.DateTo, out var dateTo))
                    {
                        Console.WriteLine("'Date to' has incorrect format");
                        return;
                    }

                    var range = new TimeRange(dateFrom, dateTo);

                    var importer = new FileGraphImporter(opts.Path, range, new ActionParser());
                    var processor = new GraphProcessor(importer);

                    processor.Process();

                    var drawer = new ConsoleGraphDrawer();

                    drawer.Draw(processor);
                });
        }
    }
}